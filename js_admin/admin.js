//search-content---------------------------------------
$('.navbar>form>.search-content>input').focusin(()=>{
    $('.navbar>form>.search-content').css('border','0.5px solid rgba(155, 155, 155, 0.18)').css('transition','0.2s');
});

$('.navbar>form>.search-content>input').focusout(()=>{
    $('.navbar>form>.search-content').css('border','1px solid rgba(155, 155, 155, 0)').css('transition','0.2s');
});

//nav-tools---------------------------------------
$('.navbar>.nav-tools>ul>.account').click(()=>{
    $('.navbar>.nav-tools>ul>.account>ul').show().css('display','flex');
});

$(document).click(function(e) {
    var target = e.target;

    if (!$(target).is('.navbar>.nav-tools>ul>.account') && !$(target).parents().is('.navbar>.nav-tools>ul>.account')) {
        $('.navbar>.nav-tools>ul>.account>ul').hide();
    }
});
//-----------------------------------------------
$('.navbar>.nav-tools>ul>.lang').click(()=>{
    $('.navbar>.nav-tools>ul>.lang>ul').show().css('display','flex');
});

$(document).click(function(e) {
    var target = e.target;

    if (!$(target).is('.navbar>.nav-tools>ul>.lang') && !$(target).parents().is('.navbar>.nav-tools>ul>.lang')) {
        $('.navbar>.nav-tools>ul>.lang>ul').hide();
    }
});
//-----------------------------------------------
$('.navbar>.nav-tools>ul>.notification').click(()=>{
    $('.navbar>.nav-tools>ul>.notification>ul').show().css('display','flex');
});

$(document).click(function(e) {
    var target = e.target;

    if (!$(target).is('.navbar>.nav-tools>ul>.notification') && !$(target).parents().is('.navbar>.nav-tools>ul>.notification')) {
        $('.navbar>.nav-tools>ul>.notification>ul').hide();
    }
});

