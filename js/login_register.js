//login--------------------------------------------------------------------------------------------
$('.login_email').focusin(()=>{
    $('.login_email').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.login_email').focusout(()=>{
    $('.login_email').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.login_password').focusin(()=>{
    $('.login_password').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.login_password').focusout(()=>{
    $('.login_password').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});


let sh_login_click=true;

$('.login-panel>form>.form-options>label>.show-hide').click(()=>{
    if(sh_login_click==true){
        $('.login-panel>form>.form-options>label>.show-hide>.round').css('float','right');
        sh_login_click=false;
        $('.login-panel>form>.login_password>input').attr('type','text');
    }else{
        $('.login-panel>form>.form-options>label>.show-hide>.round').css('float','left');
        sh_login_click=true;
        $('.login-panel>form>.login_password>input').attr('type','password');
    }
});

//regsiter--------------------------------------------------------------------------------------------

$('.register_email').focusin(()=>{
    $('.register_email').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.register_email').focusout(()=>{
    $('.register_email').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.register_name').focusin(()=>{
    $('.register_name').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.register_name').focusout(()=>{
    $('.register_name').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.register_surname').focusin(()=>{
    $('.register_surname').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.register_surname').focusout(()=>{
    $('.register_surname').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.register_mobile').focusin(()=>{
    $('.register_mobile').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.register_mobile').focusout(()=>{
    $('.register_mobile').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.register_user').focusin(()=>{
    $('.register_user').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.register_user').focusout(()=>{
    $('.register_user').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.register_password').focusin(()=>{
    $('.register_password').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.register_password').focusout(()=>{
    $('.register_password').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.company_data1').focusin(()=>{
    $('.company_data1').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.company_data1').focusout(()=>{
    $('.company_data1').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.company_data2').focusin(()=>{
    $('.company_data2').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.company_data2').focusout(()=>{
    $('.company_data2').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.personal_data').focusin(()=>{
    $('.personal_data').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.personal_data').focusout(()=>{
    $('.personal_data').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});


let sh_register_click=true;

$('.register-panel>form>.form-options>label>.show-hide').click(()=>{
    if(sh_register_click==true){
        $('.register-panel>form>.form-options>label>.show-hide>.round').css('float','right');
        sh_register_click=false;
        $('.register-panel>form>.register_password>input').attr('type','text');
    }else{
        $('.register-panel>form>.form-options>label>.show-hide>.round').css('float','left');
        sh_register_click=true;
        $('.register-panel>form>.register_password>input').attr('type','password');
    }
});

$('.register_user>select').change(()=>{
    if($('.register_user>select').val()=="personal"){
        $('.personal_data').slideDown(100);
        $('.personal_data>input').attr('required','required');
        $('.company_data1,.company_data2').slideUp(100);
        $('.company_data1>input,.company_data2>input').removeAttr('required');
    }else if($('.register_user>select').val()=="company"){
        $('.company_data1,.company_data2').slideDown(100);
        $('.company_data1>input,.company_data2>input').attr('required','required');
        $('.personal_data').slideUp(100);
        $('.personal_data>input').removeAttr('required');
    }else{
        $('.personal_data').slideUp(100);
        $('.company_data1,.company_data2').slideUp(100);
        $('.company_data1>input,.company_data2>input').removeAttr('required');
        $('.personal_data>input').removeAttr('required');
    }
});
//recover--------------------------------------------------------------------------------------------
$('.recover_email').focusin(()=>{
    $('.recover_email').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.recover_email').focusout(()=>{
    $('.recover_email').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

//new-password---------------------------------------------------------------------------------------
$('.new_password').focusin(()=>{
    $('.new_password').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.new_password').focusout(()=>{
    $('.new_password').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});

$('.again_new_password').focusin(()=>{
    $('.again_new_password').css('border-bottom','1px solid rgb(27, 85, 226)').css('transition','0.2s');
});

$('.again_new_password').focusout(()=>{
    $('.again_new_password').css('border-bottom','1px solid rgb(224, 230, 237)').css('transition','0.2s');
});


let sh_new_pass_click=true;

$('.new-password-panel>form>.form-options>label>.show-hide').click(()=>{
    if(sh_new_pass_click==true){
        $('.new-password-panel>form>.form-options>label>.show-hide>.round').css('float','right');
        sh_new_pass_click=false;
        $('.new-password-panel>form>.new_password>input').attr('type','text');
    }else{
        $('.new-password-panel>form>.form-options>label>.show-hide>.round').css('float','left');
        sh_new_pass_click=true;
        $('.new-password-panel>form>.new_password>input').attr('type','password');
    }
});