// mobile-navigation
$('.burger-menu').on('click',()=>{
    $('.mobile-navigation').slideToggle(200);
})

// scroll-navbar
$(document).scroll(()=>{
    if(scrollY>40){
        $('.navbar').css('background-color','white').css('box-shadow','rgba(0, 0, 0, 0.205) 1px 1px 5px').css('transition','0.4s all');
        $('.navbar>a>div').removeClass('logo').addClass('logo_dark').css('transition','0.4s all');
        $('.navbar>.burger-menu').removeClass('burger-menu').addClass('burger-menu-dark').css('transition','0.4s all');
        $('.navbar>.navigation>ul>li>a').addClass('after_a');
    }else if(scrollY<=40){
        $('.navbar').css('background-color','unset').css('box-shadow','unset').css('transition','0.4s all');
        $('.navbar>a>div').removeClass('logo_dark').addClass('logo').css('transition','0.4s all');
        $('.navbar>.burger-menu-dark').removeClass('burger-menu-dark').addClass('burger-menu').css('transition','0.4s all');
        $('.navbar>.navigation>ul>li>a').removeClass('after_a');
    }
})

//comments
$(document).ready(()=>{
    $("#owl-example1").owlCarousel({
        margin: 15,
        loop: true,
        callbacks: true,
        items: 1,
        slideSpeed: 500,
        autoplay: true,
        nav: false,
        autoPlaySpeed: 500,
        smartSpeed: 500,
        autoplayTimeout: 5500,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 1,
            },
            992: {
                items: 1,
            },
            1200: {
                items: 1,
                loop: true
            }
        }
    });
});


